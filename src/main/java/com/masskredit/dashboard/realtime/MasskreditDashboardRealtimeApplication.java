package com.masskredit.dashboard.realtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class MasskreditDashboardRealtimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasskreditDashboardRealtimeApplication.class, args);
	}
}
