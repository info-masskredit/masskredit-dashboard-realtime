package com.masskredit.dashboard.realtime.model;

import org.springframework.beans.factory.annotation.Value;

public class Env {

    @Value("$[masskredit.pubnub.env]")
    private String environtment;

    @Value("$[masskredit.pubnub.local.publish.key]")
    private String publishKeyLocal;
    @Value("$[masskredit.pubnub.local.subscribe.key]")
    private String subscribeKeyLocal;


    @Value("$[masskredit.pubnub.dev.publish.key]")
    private String publishKeyDev;
    @Value("$[masskredit.pubnub.dev.subscribe.key]")
    private String subscribeKeyDev;


    @Value("$[masskredit.pubnub.prod.publish.key]")
    private String publishKeyProd;
    @Value("$[masskredit.pubnub.prod.subscribe.key]")
    private String subscribeKeyProd;


}
