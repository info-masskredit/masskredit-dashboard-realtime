package com.masskredit.dashboard.realtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Base64;

@Controller
public class HomeController {

    private final Environment environment;
    private String publishKey;
    private String subscribeKey;

    @Autowired
    public HomeController(Environment environment) {
        this.environment = environment;
    }


    @RequestMapping(path = "/")
    public String index(Model model) {
        String env=environment.getProperty("masskredit.pubnub.env");
        switch (env) {
            case "local":
                publishKey = environment.getProperty("masskredit.pubnub.local.publish.key");
                subscribeKey = environment.getProperty("masskredit.pubnub.local.subscribe.key");
                break;
            case "develop":
                publishKey = environment.getProperty("masskredit.pubnub.dev.publish.key");
                subscribeKey = environment.getProperty("masskredit.pubnub.dev.subscribe.key");
                break;
            case "production":
                publishKey = environment.getProperty("masskredit.pubnub.prod.publish.key");
                subscribeKey = environment.getProperty("masskredit.pubnub.prod.subscribe.key");
                break;
        }
        model.addAttribute("publishKey",Base64.getEncoder().encodeToString(publishKey.getBytes()));
        model.addAttribute("subscribeKey",Base64.getEncoder().encodeToString(subscribeKey.getBytes()));
        return "index";

    }

}
